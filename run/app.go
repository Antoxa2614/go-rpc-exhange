package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
	"os"

	"gitlab.com/antoxa2614/go-rpc-exhange/internal/modules/rates/service"
	"google.golang.org/grpc"

	"gitlab.com/antoxa2614/go-rpc-exhange/internal/workers"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/antoxa2614/go-rpc-exhange/config"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/db"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/cache"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/component"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/db/migrate"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/db/scanner"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/errors"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/responder"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/router"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/server"
	internal "gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/service"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/tools/cryptography"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/models"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/modules"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/provider"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/storages"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf          config.AppConf
	logger        *zap.Logger
	srv           server.Server
	gRPC          server.Server
	Sig           chan os.Signal
	Storages      *storages.Storages
	Servises      *modules.Services
	MessageBroker provider.Producerer
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запуск worker'а
	exchWorker := workers.NewExchangeRates(a.Servises.Exchange, a.Storages.Rates, a.MessageBroker)
	exchWorker.Run(ctx, a.logger)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем grpc сервер
	errGroup.Go(func() error {
		err := a.gRPC.Serve(ctx)
		if err != nil {
			a.logger.Error("app: grpc server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация RabbitMQ Producer
	a.MessageBroker = provider.NewProducer(
		a.conf.RabbitMQ.Login,
		a.conf.RabbitMQ.Password,
		a.conf.RabbitMQ.Host,
		a.conf.RabbitMQ.Port,
		a.logger,
	)
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.RatesDTO{},
		&models.MinRatesDTO{},
		&models.MaxRatesDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}
	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	// инициализация сервиса Rates в gRPC
	gRPCServer := grpc.NewServer()
	service.RegisterRatesServer(gRPCServer, a.Servises.Rates)
	// инициализация сервера gRPC
	a.gRPC = server.NewGRPC(a.conf.GRPCServer, gRPCServer, a.logger)

	controllers := modules.NewControllers(services, components)

	// инициализация роутера
	var r *chi.Mux = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
