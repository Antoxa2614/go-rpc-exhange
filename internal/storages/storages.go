package storages

import (
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/db/adapter"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/cache"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/modules/rates/storage"
)

type Storages struct {
	Rates storage.Rater
	//User   ustorage.Userer
	//Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Rates: storage.NewRateStorage(sqlAdapter),
		//User:   ustorage.NewUserStorage(sqlAdapter, cache),
		//Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
