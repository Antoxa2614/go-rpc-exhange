package server

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/antoxa2614/go-rpc-exhange/config"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type ServerGRPC struct {
	conf   config.GRPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPC(conf config.GRPCServer, srv *grpc.Server, logger *zap.Logger) Server {
	return &ServerGRPC{conf: conf, logger: logger, srv: srv}
}

func (s *ServerGRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		var l net.Listener
		l, err = net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("grpc server register error", zap.Error(err))
			chErr <- err
			return
		}

		s.logger.Info("grpc server started", zap.String("port", s.conf.Port))
		err = s.srv.Serve(l)
		if err != nil {
			s.logger.Error("grpc server starting error", zap.Error(err))
			chErr <- err
			return
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
		s.logger.Error("grpc: stopping server")
	}

	return err
}
