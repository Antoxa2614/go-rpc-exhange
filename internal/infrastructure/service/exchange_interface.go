package service

import (
	"io"
	"net/http"
	"time"

	"gitlab.com/antoxa2614/go-rpc-exhange/internal/models"

	"go.uber.org/zap"
)

const base_url = "https://api.exmo.com/v1.1"

type Exchenger interface {
	Ticker() models.Rates
}

type Exchange struct {
	logger *zap.Logger
}

func NewExchange(logger *zap.Logger) *Exchange {
	return &Exchange{logger: logger}
}

func (e *Exchange) Ticker() models.Rates {
	resp, err := http.Get(base_url + "/ticker")
	if err != nil {
		e.logger.Error(err.Error())
		return models.Rates{}
	}
	defer func() { _ = resp.Body.Close() }()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		e.logger.Error(err.Error())
		return models.Rates{}
	}

	return models.Rates{
		Ticker:    string(data),
		CreatedAt: time.Now(),
	}
}
