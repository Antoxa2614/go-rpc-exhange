package models

import "time"

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type RatesDTO struct {
	ID        int32     `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Ticker    string    `json:"ticker" db:"ticker" db_ops:"create" db_type:"varchar" db_default:"not null"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_ops:"create" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func (r *RatesDTO) TableName() string {
	return "rates"
}

func (r *RatesDTO) OnCreate() []string {
	return []string{}
}
