package models

import (
	"time"
)

//go:generate easytags $GOFILE json,db,db_ops,db_type,db_default,db_index

type MaxRatesDTO struct {
	ID        int32     `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Symbol    string    `json:"symbol" db:"symbol" db_ops:"create,update" db_type:"varchar(20)" db_default:"not null"`
	Price     float64   `json:"price" db:"price" db_ops:"create,update" db_type:"numeric" db_default:"not null"`
	UpdatedAt time.Time `json:"updated_at" db:"updated_at" db_ops:"create,update" db_type:"timestamp" db_default:"default (now()) not null"`
}

func (m *MaxRatesDTO) TableName() string {
	return "max_rates"
}

func (m *MaxRatesDTO) OnCreate() []string {
	return []string{}
}

func (m *MaxRatesDTO) SetID(id int32) *MaxRatesDTO {
	m.ID = id
	return m
}

func (m *MaxRatesDTO) GetID() int32 {
	return m.ID
}

func (m *MaxRatesDTO) SetSymbol(symbol string) *MaxRatesDTO {
	m.Symbol = symbol
	return m
}

func (m *MaxRatesDTO) GetSymbol() string {
	return m.Symbol
}

func (m *MaxRatesDTO) SetPrice(price float64) *MaxRatesDTO {
	m.Price = price
	return m
}

func (m *MaxRatesDTO) GetPrice() float64 {
	return m.Price
}

func (m *MaxRatesDTO) SetUpdatedAt(t time.Time) *MaxRatesDTO {
	m.UpdatedAt = t
	return m
}

func (m *MaxRatesDTO) GetUpdatedAt() time.Time {
	return m.UpdatedAt
}
