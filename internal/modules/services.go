package modules

import (
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/component"
	eservice "gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/service"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/modules/rates/service"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/storages"
)

type Services struct {
	Exchange eservice.Exchenger
	Rates    service.RatesServer
	//User uservice.Userer
	//Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	//userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		Exchange: eservice.NewExchange(components.Logger),
		Rates:    service.NewRatesService(storages.Rates, components.Logger),
		//User: userService,
		//Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
