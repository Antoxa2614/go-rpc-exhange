package storage

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/antoxa2614/go-rpc-exhange/internal/infrastructure/db/scanner"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/db/adapter"
	"gitlab.com/antoxa2614/go-rpc-exhange/internal/models"
)

type RateStorage struct {
	adapter *adapter.SQLAdapter
}

func NewRateStorage(adapter *adapter.SQLAdapter) *RateStorage {
	return &RateStorage{adapter: adapter}
}

func (r *RateStorage) AddTicker(ctx context.Context, ticker models.RatesDTO) error {
	return r.adapter.Create(ctx, &ticker)
}

func (r *RateStorage) GetLastTicker(ctx context.Context) (string, error) {
	var result []models.RatesDTO
	var table models.RatesDTO
	tableName := table.TableName()
	err := r.adapter.List(ctx, &result, tableName, adapter.Condition{
		Order:       []*adapter.Order{{Field: "id", Asc: false}},
		LimitOffset: &adapter.LimitOffset{Limit: 1},
	})
	if err != nil {
		return "", err
	}
	return result[0].Ticker, nil
}

func (r *RateStorage) GetMinPrices(ctx context.Context) []models.MinRatesDTO {
	result := make([]models.MinRatesDTO, 0)
	var t models.MinRatesDTO
	err := r.adapter.List(ctx, &result, t.TableName(), adapter.Condition{})
	if err != nil {
		fmt.Printf("get min prices error: %s\n", err)
		return nil
	}
	return result
}

func (r *RateStorage) GetMaxPrices(ctx context.Context) []models.MaxRatesDTO {
	result := make([]models.MaxRatesDTO, 0)
	var t models.MaxRatesDTO
	err := r.adapter.List(ctx, &result, t.TableName(), adapter.Condition{})
	if err != nil {
		fmt.Printf("get max prices error: %s\n", err)
		return nil
	}
	return result
}

func (r *RateStorage) CreateMaxPrice(ctx context.Context, symbol string, price float64) error {
	data := models.MaxRatesDTO{
		Symbol:    symbol,
		Price:     price,
		UpdatedAt: time.Now(),
	}
	err := r.adapter.Create(ctx, &data)
	if err != nil {
		return err
	}
	return nil
}

func (r *RateStorage) CreateMinPrice(ctx context.Context, symbol string, price float64) error {
	data := models.MinRatesDTO{
		Symbol:    symbol,
		Price:     price,
		UpdatedAt: time.Now(),
	}
	err := r.adapter.Create(ctx, &data)
	if err != nil {
		return err
	}
	return nil
}

func (r *RateStorage) UpdateMaxPrice(ctx context.Context, symbol string, price float64) error {
	data := models.MaxRatesDTO{
		Symbol:    symbol,
		Price:     price,
		UpdatedAt: time.Now(),
	}
	err := r.adapter.Update(ctx, &data, adapter.Condition{Equal: sq.Eq{"symbol": symbol}}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (r *RateStorage) UpdateMinPrice(ctx context.Context, symbol string, price float64) error {
	data := models.MinRatesDTO{
		Symbol:    symbol,
		Price:     price,
		UpdatedAt: time.Now(),
	}
	err := r.adapter.Update(ctx, &data, adapter.Condition{Equal: sq.Eq{"symbol": symbol}}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}
