package service

import (
	"context"
	"encoding/json"

	"gitlab.com/antoxa2614/go-rpc-exhange/internal/modules/rates/storage"
	"go.uber.org/zap"
)

type RatesService struct {
	repository storage.Rater
	logger     *zap.Logger
}

func NewRatesService(repository storage.Rater, logger *zap.Logger) *RatesService {
	return &RatesService{repository: repository, logger: logger}
}

func (r *RatesService) GetTicker(ctx context.Context, in *GetTickerIn) (*GetTickerOut, error) {
	data := make(map[string]*Symbol)
	ticker, err := r.repository.GetLastTicker(ctx)
	if err != nil {
		r.logger.Warn("getting ticker error", zap.Error(err))
		return nil, err
	}
	err = json.Unmarshal([]byte(ticker), &data)
	if err != nil {
		r.logger.Warn("unmarshal ticker error", zap.Error(err))
		return nil, err
	}
	return &GetTickerOut{Rates: data}, nil
}

func (r *RatesService) GetMinPrices(ctx context.Context, in *GetMinPricesIn) (*GetMinPricesOut, error) {
	ratesMap := make(map[string]float64)
	rates := r.repository.GetMinPrices(ctx)
	for _, rate := range rates {
		ratesMap[rate.Symbol] = rate.Price
	}
	return &GetMinPricesOut{MinPrices: ratesMap}, nil
}

func (r *RatesService) GetMaxPrices(ctx context.Context, in *GetMaxPricesIn) (*GetMaxPricesOut, error) {
	ratesMap := make(map[string]float64)
	rates := r.repository.GetMaxPrices(ctx)
	for _, rate := range rates {
		ratesMap[rate.Symbol] = rate.Price
	}
	return &GetMaxPricesOut{MaxPrices: ratesMap}, nil
}

func (r *RatesService) GetAveragePrices(ctx context.Context, in *GetAveragePricesIn) (*GetAveragePricesOut, error) {
	minPrices, _ := r.GetMinPrices(ctx, &GetMinPricesIn{})
	maxPrices, _ := r.GetMaxPrices(ctx, &GetMaxPricesIn{})
	ratesMap := make(map[string]float64)
	for symbol, minPrice := range minPrices.MinPrices {
		maxPrice, ok := maxPrices.MaxPrices[symbol]
		if ok {
			ratesMap[symbol] = (minPrice + maxPrice) / 2
		}
	}
	return &GetAveragePricesOut{AvgPrices: ratesMap}, nil
}

func (r *RatesService) mustEmbedUnimplementedRatesServer() {
	//TODO implement me
	panic("implement me")
}
