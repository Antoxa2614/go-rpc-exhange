package provider

import (
	"context"
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type Producerer interface {
	Send(ctx context.Context, message string) error
}

type Producer struct {
	ch    *amqp.Channel
	queue *amqp.Queue
}

func NewProducer(login, password, host, port string, logger *zap.Logger) *Producer {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", login, password, host, port))
	if err != nil {
		logger.Fatal("RabbitMQ connection error", zap.Error(err))
	}
	//defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal("RabbitMQ create channel error", zap.Error(err))
	}
	//defer ch.Close()

	q, err := ch.QueueDeclare(
		"message", // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		logger.Fatal("RabbitMQ make queue error", zap.Error(err))
	}
	return &Producer{ch: ch, queue: &q}
}

func (p *Producer) Send(ctx context.Context, message string) error {
	err := p.ch.PublishWithContext(ctx,
		"",           // exchange
		p.queue.Name, // routing key
		false,        // mandatory
		false,        // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		})
	return err
}
